#+TITLE: An Example Title
#+AUTHOR: Vagrant Cascadian
#+EMAIL: vagrant@debian.org
#+DATE: Example Conference, Example Date
#+LANGUAGE:  en
#+OPTIONS:   H:1 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: ^:nil
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
#+latex_header: \mode<beamer>{\usetheme{Madrid}}
#+LaTeX_CLASS_OPTIONS: [aspectratio=169]
#+BEGIN_comment
INSERT ABSTRACT, location, time, etc.
#+END_comment

* Who am I

** image
	:PROPERTIES:
	:BEAMER_col: 0.4
	:END:

[[./images/vagrantupsidedown.png]]


** text
	:PROPERTIES:
	:BEAMER_col: 0.4
	:END:

  |                     | Vagrant |
  |---------------------+---------|
  | debian user         |    2001 |
  | debian developer    |    2010 |

* Example slide with bullet items

  Lorem ipsum

  #+ATTR_BEAMER: :overlay <+->
- ...
- item1
- item2
- item3

* Example columnated slide with images

 text

 maybe a link

** image
	:PROPERTIES:
	:BEAMER_col: 0.3
	:END:

[[./images/someimage.png]]

** text
	:PROPERTIES:
	:BEAMER_col: 0.6
	:END:

some more text

* Copyright and attributions
\addtocounter{framenumber}{-1}
\tiny

  Copyright 2021 Vagrant Cascadian <vagrant@reproducible-builds.org>

  This work is licensed under the Creative Commons
  Attribution-ShareAlike 4.0 International License.

  To view a copy of this license, visit
  https://creativecommons.org/licenses/by-sa/4.0/
